# Brings in files from other directories
import sys
sys.path.append("../PDT-Backend")
sys.path.append("./PDT-Backend")

from unittest import TestCase, main
from bank import Accountant
from datetime import date
from random import randint
from decimal import Decimal

class BankTest(TestCase):

    def test_bank_get_methods(self):
        '''
        Tests
            Getting correct values from
            Accountant.get_transaction_name
            Accountant.get_transaction_amount
            Accountant.get_transaction_date
            Accountant.get_transaction_remarks
        '''

        test_accountant : Accountant = Accountant()

        begin_date : date = date(2020, 8, 1)
        end_date : date = date(2020, 8, 30)
        travel_per_diem : Decimal = Decimal(200.0)
        daily_per_diem : Decimal = Decimal(140.0)

        test_accountant.set_per_diem_data(begin_date=begin_date, end_date=end_date,\
            travel_per_diem=travel_per_diem, daily_per_diem=daily_per_diem)

        test_name : str = "Testingtons"
        test_date : date = date(2020, 8, 10)
        test_amount : Decimal = Decimal(39.99)
        test_remarks : str = "This is a test"

        test_accountant.add_transaction(name=test_name, transaction_date=test_date,\
            amount=test_amount, remarks=test_remarks)

        # test retrieving values
        self.assertEqual(test_accountant.get_transaction_name(transaction_index=0), test_name)
        self.assertEqual(test_accountant.get_transaction_date(transaction_index=0), test_date)
        self.assertEqual(test_accountant.get_transaction_amount(transaction_index=0), test_amount)
        self.assertEqual(test_accountant.get_transaction_remarks(transaction_index=0), test_remarks)

    def test_bank_get_exceptions(self):
        '''
        Test
            Asking to "get" values from a transaction out of bounds
            raises and indexerror Exception
            Accountant.get_transaction_name
            Accountant.get_transaction_amount
            Accountant.get_transaction_date
            Accountant.get_transaction_remarks
        '''

        test_accountant : Accountant = Accountant()

        # test 100 random indexes from 0 to 100
        for i in range(1, 100):
            n : int = randint(0, 100)

            with self.assertRaises(IndexError):
                test_accountant.get_transaction_name(n)

            with self.assertRaises(IndexError):
                test_accountant.get_transaction_date(n)

            with self.assertRaises(IndexError):
                test_accountant.get_transaction_amount(n)

            with self.assertRaises(IndexError):
                test_accountant.get_transaction_remarks(n)

main()

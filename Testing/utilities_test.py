# Brings in files from other directories
import sys
sys.path.append("../PDT-Backend")
sys.path.append("./PDT-Backend")

from unittest import TestCase, main
from utilities import Utils
from datetime import date
from random import randint
from decimal import Decimal

class UtilsTest(TestCase):

    def test_convert_to_date_object(self):
        '''
        Tests
            convert integer to date object
        '''

        test_date : int = 20201225

        # test conversion
        self.assertEqual(Utils.convert_to_date_object(test_date), date(2020, 12, 25))

main()

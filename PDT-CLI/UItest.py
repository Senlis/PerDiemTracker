import unittest
from UI import TextUI
import datetime
from bank import InvalidOperationError, Transaction
from random import randint
from decimal import Decimal

# todo: can we divorce accountant class from testUI class?

class UITests(unittest.TestCase):
    def setUp(self):
        self.testUI = TextUI(False)  # Create test UI, False flag prevents loading from file
        number_test_transactions = 45

        # This data must be created for test to proceed
        begin_date = datetime.date(2016, 8, 15)
        end_date = datetime.date(2016, 8, 26)
        travel_per_diem = Decimal(200.0)
        daily_per_diem = Decimal(140.0)

        self.testUI.bill.set_per_diem_data(begin_date, end_date, travel_per_diem,
                                 daily_per_diem)

        # create test transactions
        try:
            for i in range(1, number_test_transactions):
                name = "Test" + str(i)
                random_date_delta = randint(0, self.testUI.bill.calculate_trip_duration().days)
                date = begin_date + datetime.timedelta(0, 0, random_date_delta)
                amount = Decimal(20.0 + i)
                remarks = "This is the " + str(i) + " test"
                self.testUI.bill.add_transaction(name, date, amount, remarks)
        except InvalidOperationError as e:
            print(e.message)

    def test_page_up_down_correction(self):
        """
        Test ensures page up and down works and does not go out of bounds
        args: none
        returns: none
        raises:
            InvalidOperationError - transaction data is malformed
        """

        # run tests
        # transactions index runs from 0 to 44 for a total of 45 transactions
        self.assertEqual(self.testUI.display_index, 0) # test display index starting point
        self.testUI.page_up_down(is_up=False) # display index changes from 0 to 20
        self.assertEqual(self.testUI.display_index, 20)
        self.testUI.page_up_down(is_up=False) # display index changes from 20 to 40, corrected to 24
        self.assertEqual(self.testUI.display_index, 24)
        self.testUI.page_up_down(is_up=True) # display index changes from 24 to 4
        self.assertEqual(self.testUI.display_index, 4)
        self.testUI.page_up_down(is_up=True) # display index changes from 4 to -16, corrected to 0
        self.assertEqual(self.testUI.display_index, 0)

    def test_change_per_diem_data(self):
        # step 1: create test data
        test_begin_date = datetime.date(2016, 8, 15)
        test_end_date = datetime.date(2016, 8, 26)
        test_travel_per_diem = Decimal(200.0)
        test_daily_per_diem = Decimal(140.0)

        # step 2: set per diem amounts to test data

        # todo: test function with garbage data
        self.testUI.bill.set_per_diem_data(test_begin_date, test_end_date, test_travel_per_diem, test_daily_per_diem)

        # step 3: pull changed per diem amounts
        new_begin_date = self.testUI.bill.get_trip_value("begin date")
        new_end_date = self.testUI.bill.get_trip_value("end date")
        new_travel_per_diem = self.testUI.bill.get_trip_value("travel per diem")
        new_daily_per_diem = self.testUI.bill.get_trip_value("daily per diem")

        # step 4: verify new per diem values match test per diem values
        self.assertEqual(test_begin_date, new_begin_date)
        self.assertEqual(test_end_date, new_end_date)
        self.assertEqual(test_travel_per_diem, new_travel_per_diem)
        self.assertEqual(test_daily_per_diem, new_daily_per_diem)

    def test_change_transaction_data(self):
        # step 1: create test data
        test_transaction_number = 2
        test_name = "Unit Test"
        test_amount = Decimal(12.34)
        test_date = datetime.date(2016, 8, 16)
        test_remarks = "This is a modified transaction"

        # step 2: set transaction amount to test data
        self.testUI.bill.modify_transaction(test_transaction_number, test_name, test_date, test_amount, test_remarks)

        # step 3: try to create identical transaction
        with self.assertRaises(InvalidOperationError):
            test_transaction = Transaction(test_name, test_date, test_amount, test_remarks)
            self.testUI.bill.find_transaction(test_transaction) # todo: exception is not raised when it should be

        # todo: test function with garbage data

        '''
        This technique does not work since transaction moves
        # step 3: pull changed transaction values
        new_name = self.testUI.bill.get_transaction_value("name", test_transaction_number)
        new_date = self.testUI.bill.get_transaction_value("date", test_transaction_number)
        new_amount = self.testUI.bill.get_transaction_value("amount", test_transaction_number)
        new_remarks = self.testUI.bill.get_transaction_value("remarks", test_transaction_number)

        # step 4: test new values
        self.assertEqual(test_name, new_name)
        self.assertEqual(test_amount, new_amount)
        self.assertEqual(test_date, new_date)
        self.assertEqual(test_remarks, new_remarks)
        '''

unittest.main()

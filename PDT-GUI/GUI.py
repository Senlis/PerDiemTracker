# The next two lines will include the backend folder to search for project files
import sys
sys.path.append("../PDT-Backend") # this is where bank file will be while working on the program
sys.path.append("./PDT-Backend") # this is where the bank file will be after the program is built using pyinstaller

from bank import Accountant, InvalidOperationError, UserQuitException
import tkinter as tk
from decimal import Decimal, getcontext
import datetime

# todo: go through all files and only import what is necessary

class Style_Sheet:
    def __init__(self):
        self.black_bg : str = "#121212"
        self.purple_bg : str = "#1F1B24"

class Per_Diem_First_Entry:
    def __init__(self, theme: Style_Sheet):
        self.__root : tk.Tk = tk.Tk()

        self.__main_frame : tk.Frame = tk.Frame(self.__root, bg = theme.black_bg)

        self.__author_label : tk.Label = tk.Label(self.__main_frame, text = "Enter per diem data", 
            font = ("Helvetica", 16), bg = theme.black_bg, fg = "white", width = 20, height = 1)
        self.__start_label : tk.Label = tk.Label(self.__main_frame, text = "Start Date", 
            font = ("Helvetica", 16), bg = theme.black_bg, fg = "white", width = 20, height = 1)           
        self.__end_label : tk.Label = tk.Label(self.__main_frame, text = "Start Date", 
            font = ("Helvetica", 16), bg = theme.black_bg, fg = "white", width = 20, height = 1) 
        self.__travel_label : tk.Label = tk.Label(self.__main_frame, text = "Travel Per Diem", 
            font = ("Helvetica", 16), bg = theme.black_bg, fg = "white", width = 20, height = 1)
        self.__daily_label : tk.Label = tk.Label(self.__main_frame, text = "Daily Per Diem", 
            font = ("Helvetica", 16), bg = theme.black_bg, fg = "white", width = 20, height = 1)

        self.__start_entry : tk.Entry = tk.Entry()
        self.__end_entry: tk.Entry = tk.Entry()
        self.__travel_entry: tk.Entry = tk.Entry()
        self.__daily_entry : tk.Entry = tk.Entry()

        self.__OK_button: tk.Button = tk.Button(self.__main_frame, text = "OK", font = ("Helvetica", 25),
            borderwidth = 0, bg = theme.black_bg, fg = "white", highlightthickness = 0)

        self.__author_label.grid(row = 0, column = 0, columnspan = 2)

        self.__main_frame.pack()

        self.__root.protocol("WM_DELETE_WINDOW", self.__window_close_button_pressed)
        self.__root.mainloop()

    def __window_close_button_pressed(self):
        self.next_screen = "quit"

        self.__root.quit()

    def self_destruct(self):
        self.__root.destroy()

class Splash_Screen:
    def __init__(self, theme : Style_Sheet):
        self.__root : tk.Tk = tk.Tk()

        self.__main_frame : tk.Frame = tk.Frame(self.__root, bg = theme.black_bg)

        self.__top_frame : tk.Frame = tk.Frame(self.__main_frame, bg = theme.purple_bg)
        self.__bottom_frame : tk.Frame = tk.Frame(self.__main_frame, bg = theme.purple_bg)

        self.__title_label : tk.Label = tk.Label(self.__top_frame, text = "Per Diem Tracker", 
            font = ("Helvetica", 32), bg = theme.purple_bg, fg = "white", width = 20, height = 1)
        self.__author_label : tk.Label = tk.Label(self.__top_frame, text = "By Richard Romick", 
            font = ("Helvetica", 16), bg = theme.purple_bg, fg = "white", width = 20, height = 1)
        self.__title_label.pack(pady = 10)
        self.__author_label.pack(pady = 30)

        self.__continue_button: tk.Button = tk.Button(self.__bottom_frame, text = "Continue", font = ("Helvetica", 25),
            borderwidth = 0, bg = theme.purple_bg, fg = "white", highlightthickness = 0, command = self.__continue_button_click)
        self.__continue_button.pack(pady = 50, padx = 200)

        self.__top_frame.pack(padx = 20, pady = 20)
        self.__bottom_frame.pack(padx = 20, pady = 20)
        self.__main_frame.pack()

        self.__root.protocol("WM_DELETE_WINDOW", self.__window_close_button_pressed)
        self.__root.mainloop()

    def __continue_button_click(self):
        self.next_screen = "Per_Diem_First_Entry"

        self.__root.quit()

    def __window_close_button_pressed(self):
        self.next_screen = "quit"

        self.__root.quit()

    def self_destruct(self):
        self.__root.destroy()

class GUI:
    def __init__(self):
        self.__theme : Style_Sheet = Style_Sheet()

    def start_gui(self):
        self.next_screen : str = "Splash_Screen"

        while self.next_screen != "quit":

            if self.next_screen == "Splash_Screen":
                self.__splash_window : Splash_Screen = Splash_Screen(theme = self.__theme)
                self.next_screen = self.__splash_window.next_screen # get the next screen
                self.__splash_window.self_destruct()
            elif self.next_screen == "Per_Diem_First_Entry":
                self.__per_diem_entry : Per_Diem_First_Entry = Per_Diem_First_Entry(theme = self.__theme)
                self.next_screen = self.__per_diem_entry.next_screen
                self.__per_diem_entry.self_destruct()
            else:
                # todo: print to console
                self.next_screen = "quit"

        

user_interface : GUI = GUI()
user_interface.start_gui()
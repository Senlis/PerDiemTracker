# PerDiemCalculator

This app is designed to calculate your total per diem for a TDY and track your transactions.  As you enter transactions, the app will inform you of how much per diem money you have spent against how much of the TDY has passed.

[Download current release here](https://gitlab.com/Senlis/PerDiemTracker/-/tree/master/Releases)

#### Current capabilities
* Text based UI
* Track per diem and transactions for 1 TDY at a time

For future capabilities, see the [Roadmap](https://gitlab.com/Senlis/PerDiemTracker/-/wikis/Roadmap)

#### How to use app

Download the [current release](https://gitlab.com/Senlis/PerDiemTracker/-/tree/master/Releases) for your platform (Windows, Mac OS X or Linux).  Unzip the downloaded archive, and double click the "start_program" file.  The "start_program" file will have a ".exe" extension on Windows.

#### Issues and bugs

If you find any issues or bugs, please send them to the [Help Desk](incoming+senlis-perdiemtracker-7300562-issue-@incoming.gitlab.com)

#### Final notes

Currently, the only version of the app available uses text.  This version is meant for testing, but isn't meant to be the ideal user interface.  Android and iOS versions are coming soon.